/**
 * Author: Muhammet Emin Kurt
 * Date: 10 Kas�m 2020
 * Source: HackerRank - https://www.hackerrank.com/challenges/c-tutorial-class/problem
 **/
#include <iostream>
#include <sstream>
using namespace std;
/**
*
* @brief Includes Students' Informations
*/
class Student {
private:
    int age;
    string first_name;
    string last_name;
    int standard;

public:
 /**
 *
 * @brief sets age
 */
    void set_age(int a) {
        age = a;
    }
/**
*
* @brief sets standard deviation
*/
    void set_standard(int b) {
        standard = b;
    }
/**
*
* @brief sets first name
*/
    void set_first_name(string c) {
        first_name = c;
    }
/**
*
* @brief sets last name
*/
    void set_last_name(string d) {
        last_name = d;
    }
/**
* @return age returns student's age.
*
* @brief gets age
*/
    int get_age() {
        return age;
    }
/**
* @return last_name returns student's last name.
*
* @brief gets last name
*/
    string get_last_name() {
        return last_name;
    }
    /**
    * @return first_name returns student's first name.
    *
    * @brief gets first name
    */
    string get_first_name() {
        return first_name;
    }
/**
* @return age returns standard deviation.
*
* @brief gets standard
*/
    int get_standard() {
        return standard;
    }
    string to_string() {
        char ch = ',';
        string str, s;
        stringstream ss(str);
        ss << age << ch << first_name << ch << last_name << ch << standard;
        ss >> s;
        return s;
    }
};

int main() {
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);

    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();

    return 0;
}
