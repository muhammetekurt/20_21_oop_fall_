/**
 * Author: Muhammet Emin Kurt
 * Date: 10 Kas�m 2020
 * Source: HackerRank - https://www.hackerrank.com/challenges/cpp-input-and-output/problem
 **/
#include <iostream>

using namespace std;

int main() 
{
    int a, b, c;
    int sum = 0;

    cin >> a >> b >> c;

    cout << a + b + c;

    return 0;
}