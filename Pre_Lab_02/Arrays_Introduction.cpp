/**
 * Author: Muhammet Emin Kurt
 * Date: 10 Kas�m 2020
 * Source: HackerRank - https://www.hackerrank.com/challenges/arrays-introduction/problem
 **/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {

    int size;
    cin >> size;
    int A[size];

    
    for (int i = 0; i < size; i++)
    {
        cin >> A[i];
    }
    // We printed the array elements backwards
    for (int i = size - 1; i >= 0; i--) {
        cout << A[i] << " ";
    }
    return 0;
}
