/**
 * Author: Muhammet Emin Kurt
 * Date: 10 Kas�m 2020
 * Source: HackerRank - https://www.hackerrank.com/challenges/c-tutorial-strings/problem
 **/
#include <iostream>
#include <string>
using namespace std;

int main() {

    string m, n;
    cin >> m;
    cin >> n;

    string temp;
    int a, b;

    a = m.size();
    b = n.size();

    cout << a << " " << b << endl;
    cout << m + n << endl;

    temp[0] = m[0];
    m[0] = n[0];
    n[0] = temp[0];
    cout << m << " " << n << endl;

    return 0;
}
