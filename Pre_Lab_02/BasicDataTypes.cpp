/**
 * Author: Muhammet Emin Kurt
 * Date: 10 Kas�m 2020
 * Source: HackerRank - https://www.hackerrank.com/challenges/c-tutorial-basic-data-types/problem
 **/
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <cstdio>
using namespace std;

int main() {
    int a;
    long l;
    char c;
    float f;
    double d;

    scanf("%d %ld %c %f %lf", &a, &l, &c, &f, &d);

    printf("%d\n", a);
    printf("%ld\n", l);
    printf("%c\n", c);
    printf("%f\n", f);
    printf("%lf\n", d);


    return 0;
}