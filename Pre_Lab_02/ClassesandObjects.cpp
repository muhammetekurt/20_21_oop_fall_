/**
 * Author: Muhammet Emin Kurt
 * Date: 10 Kas�m 2020
 * Source: HackerRank - https://www.hackerrank.com/challenges/classes-objects/problem
 **/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cassert>
using namespace std;
/**
*
* @brief Includes Students' scores and their scores' summation.
*/
class Student
{
private:
    int scores[5];
public:
 /**
 *
 * @brief Defines array elements
 */
    void input()
    {
        for (int i = 0; i < 5; i++)
        {
            cin >> scores[i];
        }
    }
    /**
 *
 * @return sum is: summation of elements of array.
 * @brief Calculates sum of students' total scores.
 */
    int calculateTotalScore()
    {
        int sum;
        for (int i = 0; i < 5; i++)
        {
            sum = sum + scores[i];
        }
        return sum;
    }
};
/**
 *
 * @brief Main function.
 */
int main() {
    int n; // number of students
    cin >> n;
    Student* s = new Student[n]; // an array of n students

    for (int i = 0; i < n; i++) {
        s[i].input();
    }

    // calculate kristen's score
    int kristen_score = s[0].calculateTotalScore();

    // determine how many students scored higher than kristen
    int count = 0;
    for (int i = 1; i < n; i++) {
        int total = s[i].calculateTotalScore();
        if (total > kristen_score) {
            count++;
        }
    }

    // print result
    cout << count;

    return 0;
}
