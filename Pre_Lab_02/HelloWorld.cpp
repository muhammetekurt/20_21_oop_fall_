/**
*@mainpage Project Title
*This is the main page for the project.
*
*1. Hackerrank solution codes for the first 17 problems (For C++)
*2. NOTE!!!!!: Inherited Code question not resolved..
*
*3. LINKS:
*4. https://www.hackerrank.com/challenges/cpp-hello-world/problem
*5. https://www.hackerrank.com/challenges/cpp-input-and-output/problem
*6. https://www.hackerrank.com/challenges/c-tutorial-basic-data-types/problem
*7. https://www.hackerrank.com/challenges/c-tutorial-conditional-if-else/problem
*8. https://www.hackerrank.com/challenges/c-tutorial-for-loop/problem
*9. https://www.hackerrank.com/challenges/c-tutorial-functions/problem
*10. https://www.hackerrank.com/challenges/c-tutorial-pointer/problem
*11. https://www.hackerrank.com/challenges/arrays-introduction/problem
*12. https://www.hackerrank.com/challenges/variable-sized-arrays/problem
*13. https://www.hackerrank.com/challenges/attribute-parser/problem
*14. https://www.hackerrank.com/challenges/c-tutorial-stringstream/problem
*15. https://www.hackerrank.com/challenges/c-tutorial-strings/problem
*16. https://www.hackerrank.com/challenges/c-tutorial-struct/problem
*17. https://www.hackerrank.com/challenges/c-tutorial-class/problem
*18. https://www.hackerrank.com/challenges/classes-objects/problem
*19. https://www.hackerrank.com/challenges/box-it/problem
*/
/**
* @page       Personal Information
* @author   : Muhammet Emin Kurt >> Mail >> mekurt55@gmail.com >> Number >> 152120171045
* @date     : 3 Kasim 2020
* @version  : v1.0
* @Source: HackerRank - https://www.hackerrank.com/domains/cpp
*/

#include <iostream>
#include <cstdio>
using namespace std;

int main() {
    cout << "Hello, World!";
    return 0;
}