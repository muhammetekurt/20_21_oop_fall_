/**
 * Author: Muhammet Emin Kurt
 * Date: 10 Kas�m 2020
 * Source: HackerRank - https://www.hackerrank.com/challenges/c-tutorial-stringstream/problem
 **/
#include <sstream>
#include <iostream>

using namespace std;

int main()
{
    string x;
    cin >> x;

    for (int i = 0; i < x.size(); i++)
    {
        if (x[i] != ',') {
            cout << x[i];
        }
        else {
            cout << "\n";
        }
    }
    return 0;
}
