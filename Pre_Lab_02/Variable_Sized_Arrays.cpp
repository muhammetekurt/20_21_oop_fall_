/**
 * Author: Muhammet Emin Kurt
 * Date: 10 Kas�m 2020
 * Source: HackerRank - https://www.hackerrank.com/challenges/variable-sized-arrays/problem
 **/
#include <iostream>

using namespace std;

int main() {
    int length = 0;
    int m;
    int n = 0;

    cin >> m;
    cin >> n;

    int** ar = new int* [m];
    int query[2];

    for (int i = 0; i < m; i++)
    {
        cin >> length;
        ar[i] = new int[length];

        for (int j = 0; j < length; j++)
        {
            cin >> ar[i][j];
        }
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < 2; j++) {
            cin >> query[j];
        }
        cout << ar[query[0]][query[1]] << endl;
    }
    return 0;
}
