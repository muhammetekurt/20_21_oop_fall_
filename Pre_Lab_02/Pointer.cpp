/**
 * Author: Muhammet Emin Kurt
 * Date: 10 Kas�m 2020
 * Source: HackerRank - https://www.hackerrank.com/challenges/c-tutorial-pointer/problem
 **/
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
 /**
  * @param a first parameter we want to change
  * @param b second parameter we want to change
  * @brief updates numbers with their new values
  */
void update(int* a, int* b) {

    int x = *a + *b;
    int y = abs(*b - *a);
    *a = x;
    *b = y;
}

int main() {
    int a, b;
    int* pa = &a, * pb = &b;

    scanf("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}
