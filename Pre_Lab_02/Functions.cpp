/**
 * Author: Muhammet Emin Kurt
 * Date: 10 Kas�m 2020
 * Source: HackerRank - https://www.hackerrank.com/challenges/c-tutorial-functions/problem
 **/
#include <iostream>
#include <cstdio>
using namespace std;
/**
 *
 * @return max returns the largest number
 * @brief finds the largest number
 */
int max_of_four(int a, int b, int c, int d)
{
    int max = a;

    if (b > max)
        max = b;
    if (c > max)
        max = c;
    if (d > max)
        max = d;
    return max;
}
/**
 *
 * @brief main function
 */
int main() {
    int a, b, c, d;
    scanf("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d);
    printf("%d", ans);

    return 0;
}
