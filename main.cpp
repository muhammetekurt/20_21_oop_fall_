/**
*@mainpage Project Title
*This is the main page for the project.
*
*1. This program reads the given file and calculates the desired values.
*2. The information of the creator of this program is available on the "related pages".
*
*/
/**
* @page       Introductıon
* @author   : Muhammet Emin Kurt >> Mail >> mekurt55@gmail.com >> Number >> 152120171045
* @date     : 3 Kasim 2020
* @version  : v1.0
*/

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

/**
 *
 * @return sum is: summation of elements of array.
 * @brief Calculates sum of array's elements.
 */
int Sum(int* array, int n)
{
	int sum = 0;
	for (int i = 0; i < n; i++)
	{
		sum = sum + array[i];
	}
	return sum;
}
/**
 *
 * @return smallest is: smallest element of array.
 * @brief Calculates smallest element in array.
 */

int Smallest(int* array, int n)
{
	int smallest = array[0];
	for (int i = 0; i < n; i++)
	{
		if (array[i] < smallest)
		{
			smallest = array[i];
		}
	}
	return smallest;
}
/**
 *
 * @return product is: multiplication of elements of array.
 * @brief Calculates product of array's elements.
 */
int Product(int* array, int n)
{
	int product = 1;
	for (int i = 0; i < n; i++)
	{
		product = product * array[i];
	}
	return product;
}
/**
 *
 * @return avg is: average of the sum of the array elements.
 * @brief Calculates average of array's elements.
 */

float Average(int* array, int n)
{
	float avg = 0.0;

	avg = float(Sum(array, n)) / n;

	return avg;
}
/**
 *
 * @brief ReadInput function is: 
 * @brief 1) Reads integers from input.txt.
 * @brief 2) Checks the content of the file read and shuts down the system if there is an integer variable.
 * @brief 3) If all values ​​are integers: If the (number of all elements is -1! = array length (range)) then the system will fail.
 * @brief 4) To avoid problems reading values, dosyaOku.ignore (80, '\ n'); was used. This process means getting to the next line.
 * @brief 5) In addition, we always open and close the file because we did not assign incorrect values ​​to variables and want the code to run incorrectly.
 * @brief So, the file has been read and errors blocked...
 */

void ReadInput() {

	int* ptr;///< @param *ptr: We used a pointer to create a dynamic array.
	int range;///< @param range: Variable that holding the number of elements in the array.
	int A;///< @param A: An ordinary integer variable to use for error detection.
	int counter = 0, counter2 = 0;///< @param counter: Two common counters for fault finding.
	ifstream dosyaOku;///< @param dosyaOku: The variable we will use to read the file.

	dosyaOku.open("input.txt");

	if (!dosyaOku)
	{
		cout << "The file was not opened.\n\n";
	}

	else
	{
		dosyaOku >> range;

		dosyaOku.close();

		dosyaOku.open("input.txt");

		while (!dosyaOku.eof())
		{

			dosyaOku >> A;

			if (dosyaOku.fail())
			{
				cout << endl << "Wrong Input..!!" << endl;
				exit(0);
			}
			counter++;

		}

		if (counter - 1 != range)
		{
			cout << endl << "Wrong Input..!!" << endl;
			exit(0);
		}

		dosyaOku.close();

		dosyaOku.open("input.txt");

		dosyaOku >> range;

		dosyaOku.ignore(80, '\n');

		ptr = new int[range];

		for (int i = 0; i < range; i++)
		{
			dosyaOku >> ptr[i];

			if (dosyaOku.fail())
			{
				cout << endl << "Wrong Input..!!" << endl;
				exit(0);
			}

		}
		dosyaOku.close();

		dosyaOku.open("input.txt");
		A = 0;

		while (!dosyaOku.eof())
		{

			dosyaOku >> A;
			counter2++;
		}

		if (counter2 - 1 != range)
		{
			cout << endl << "Wrong Input..!!" << endl;

			exit(0);
		}
		
		cout << endl << endl;
		cout << "Sum is: " << Sum(ptr, range) << endl;
		cout << "Product is: " << Product(ptr, range) << endl;
		cout << "Average is: " << Average(ptr, range) << endl;
		cout << "Smallest is: " << Smallest(ptr, range) << endl << endl;
	}
}
/**
*
*@brief Our main function.
*/
int main()
{
	ReadInput();

	return 0;
}